using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonController : MonoBehaviour
{
    public GameObject winScreen;
    public GameObject failScreen;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartGame()
    {
        GameObject.Find("Start Button").SetActive(false);
        EventManager.StartEvent();
        Time.timeScale = 1;
    }

    public void NextScene()
    {
        PlayerPrefs.SetInt("Level", PlayerPrefs.GetInt("Level") + 1);

        if (PlayerPrefs.GetInt("Level") > SceneManager.sceneCountInBuildSettings - 1)
        {
            PlayerPrefs.SetInt("Level", 1);
        }

        SceneManager.LoadScene(0);
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
