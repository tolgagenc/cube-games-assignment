using TMPro;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public TextMeshProUGUI health;
    public TextMeshProUGUI coin;

    int inGameCoin;
    int inGameHealth;
    int totalHealth;

    ButtonController BC;

    [SerializeField]
    ParticleSystem hitParticle;
    [SerializeField]
    ParticleSystem coinParticle;

    [SerializeField]
    Animation heartImg;
    [SerializeField]
    Animation coinImg;

    // Start is called before the first frame update
    void Start()
    {
        if ( !PlayerPrefs.HasKey("Coin") )
        {
            PlayerPrefs.SetInt("Coin", 0);
        }

        GameObject.Find("Level").GetComponent<TextMeshProUGUI>().text = "Level " + PlayerPrefs.GetInt("Level").ToString();

        BC = FindObjectOfType<ButtonController>();

        totalHealth = PlayerPrefs.GetInt("TotalHealth");
        inGameCoin = PlayerPrefs.GetInt("Coin");
        inGameHealth = totalHealth;

        health.text = inGameHealth.ToString();
        coin.text = PlayerPrefs.GetInt("Coin").ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if( inGameHealth <= 0)
        {
            BC.failScreen.SetActive(true);
            Time.timeScale = 0;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Obstacle")
        {
            inGameHealth--;

            health.text = inGameHealth.ToString();

            Destroy(other.gameObject);

            EventManager.StumbleEvent();

            if (hitParticle.isPlaying)
            {
                hitParticle.Stop();
            }

            hitParticle.Play();

            heartImg.Play();
        }

        if(other.tag == "Diamond")
        {
            inGameCoin += PlayerPrefs.GetInt("DiamondIncome");

            coin.text = inGameCoin.ToString();

            Destroy(other.gameObject);

            coinImg.Play();


            if (coinParticle.isPlaying)
            {
                coinParticle.Stop();
            }

            coinParticle.Play();
        }

        if(other.tag == "Diamond5")
        {
            inGameCoin += PlayerPrefs.GetInt("Diamond5Income");

            coin.text = inGameCoin.ToString();

            Destroy(other.gameObject);

            coinImg.Play();

            if (coinParticle.isPlaying)
            {
                coinParticle.Stop();
            }

            coinParticle.Play();
        }

        if (other.tag == "Finish")
        {
            EventManager.FinishEvent();

            PlayerPrefs.SetInt("Coin", inGameCoin);

            BC.winScreen.SetActive(true);
        }
    }

    void OnApplicationQuit()
    {
        PlayerPrefs.SetInt("Coin", inGameCoin);
    }
}
