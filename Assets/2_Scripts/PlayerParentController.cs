using UnityEngine;

public class PlayerParentController : MonoBehaviour
{
    public float playerSpeed = 5f;

    bool canMove;

    private void OnEnable()
    {
        EventManager.start += StartGame;
        EventManager.finish += Finish;
    }

    private void OnDisable()
    {
        EventManager.start -= StartGame;
        EventManager.finish -= Finish;
    }

    // Start is called before the first frame update
    void Start()
    {

    }
    void StartGame()
    {
        canMove = true;
    }

    void Finish()
    {
        playerSpeed = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void FixedUpdate()
    {
        if (canMove)
        {
            transform.Translate(Vector3.forward * Time.deltaTime * playerSpeed);
        }
    }
}
