using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    [SerializeField]
    GameObject healthButton;
    [SerializeField]
    GameObject diamondButton;
    [SerializeField]
    GameObject diamond5Button;

    TextMeshProUGUI healthPrice;
    TextMeshProUGUI diamondPrice;
    TextMeshProUGUI diamond5Price;

    public TextMeshProUGUI health;
    public TextMeshProUGUI coin;

    // Start is called before the first frame update
    void Awake()
    {
        if (!PlayerPrefs.HasKey("TotalHealth") || PlayerPrefs.GetInt("TotalHealth") == 0)
        {
            PlayerPrefs.SetInt("TotalHealth", 3);
        }

        if (!PlayerPrefs.HasKey("Level") || PlayerPrefs.GetInt("Level") == 0)
        {
            PlayerPrefs.SetInt("Level", 1);
        }

        if(!PlayerPrefs.HasKey("DiamondIncome") || PlayerPrefs.GetInt("DiamondIncome") == 0)
        {
            PlayerPrefs.SetInt("DiamondIncome", 1);
        }

        if (!PlayerPrefs.HasKey("Diamond5Income") || PlayerPrefs.GetInt("Diamond5Income") == 0)
        {
            PlayerPrefs.SetInt("Diamond5Income", 3);
        }

        GameObject.Find("Level").GetComponent<TextMeshProUGUI>().text = "Level " + PlayerPrefs.GetInt("Level").ToString();
    }

    private void Start()
    {
        healthPrice = healthButton.transform.GetChild(1).GetComponent<TextMeshProUGUI>();
        diamondPrice = diamondButton.transform.GetChild(1).GetComponent<TextMeshProUGUI>();
        diamond5Price = diamond5Button.transform.GetChild(1).GetComponent<TextMeshProUGUI>();

        healthPrice.text = (PlayerPrefs.GetInt("TotalHealth") * 10).ToString();
        diamondPrice.text = (PlayerPrefs.GetInt("DiamondIncome") * 3).ToString();
        diamond5Price.text = (PlayerPrefs.GetInt("Diamond5Income") * 5).ToString();

        health.text = PlayerPrefs.GetInt("TotalHealth").ToString();
        coin.text = PlayerPrefs.GetInt("Coin").ToString();

        if (PlayerPrefs.GetInt("Coin") < PlayerPrefs.GetInt("TotalHealth") * 10)
        {
            healthButton.GetComponent<Button>().interactable = false;
        }
        if (PlayerPrefs.GetInt("Coin") < PlayerPrefs.GetInt("DiamondIncome") * 3)
        {
            diamondButton.GetComponent<Button>().interactable = false;
        }
        if (PlayerPrefs.GetInt("Coin") < PlayerPrefs.GetInt("Diamond5Income") * 5)
        {
            diamond5Button.GetComponent<Button>().interactable = false;

        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void StartGame()
    {
        GameObject.Find("Start Button").SetActive(false);
        EventManager.StartEvent();
        Time.timeScale = 1;
        SceneManager.LoadScene(PlayerPrefs.GetInt("Level"));
    }

    public void IncreaseHeart()
    {if ( PlayerPrefs.GetInt("Coin") >= PlayerPrefs.GetInt("TotalHealth") * 10)
        {
            healthButton.GetComponent<Button>().interactable = true;

            PlayerPrefs.SetInt("Coin", PlayerPrefs.GetInt("Coin") - PlayerPrefs.GetInt("TotalHealth") * 10);
            PlayerPrefs.SetInt("TotalHealth", PlayerPrefs.GetInt("TotalHealth") + 1);

            healthPrice.text = (PlayerPrefs.GetInt("TotalHealth") + 100).ToString();
        }
        else
        {
            healthButton.GetComponent<Button>().interactable = false;
        }

        health.text = PlayerPrefs.GetInt("TotalHealth").ToString();
        coin.text = PlayerPrefs.GetInt("Coin").ToString();
    }

    public void IncreaseDiamond()
    {
        if (PlayerPrefs.GetInt("Coin") >= PlayerPrefs.GetInt("DiamondIncome") * 3)
        {
            diamondButton.GetComponent<Button>().interactable = true;

            PlayerPrefs.SetInt("Coin", PlayerPrefs.GetInt("Coin") - PlayerPrefs.GetInt("DiamondIncome") * 3);
            PlayerPrefs.SetInt("DiamondIncome", PlayerPrefs.GetInt("DiamondIncome") + 1);

            diamondPrice.text = (PlayerPrefs.GetInt("DiamondIncome") + 30).ToString();
        }
        else
        {
            diamondButton.GetComponent<Button>().interactable = false;
        }

        coin.text = PlayerPrefs.GetInt("Coin").ToString();
    }

    public void Increase5Diamond()
    {
        if (PlayerPrefs.GetInt("Coin") >= PlayerPrefs.GetInt("Diamond5Income") * 5)
        {
            diamond5Button.GetComponent<Button>().interactable = true;

            PlayerPrefs.SetInt("Coin", PlayerPrefs.GetInt("Coin") - PlayerPrefs.GetInt("Diamond5Income") * 5);
            PlayerPrefs.SetInt("Diamond5Income", PlayerPrefs.GetInt("Diamond5Income") + 3);

            diamond5Price.text = (PlayerPrefs.GetInt("Diamond5Income") + 50).ToString();

        }
        else
        {
            diamond5Button.GetComponent<Button>().interactable = false;
        }

        coin.text = PlayerPrefs.GetInt("Coin").ToString();
    }
}
