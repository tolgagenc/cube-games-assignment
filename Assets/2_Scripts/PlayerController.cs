using UnityEngine;

public class PlayerController : MonoBehaviour
{
    float minRoadX = -1.5f;
    float maxRoadX = 1.5f;

    float screenDis;
    float roadDisX;
    float disDifX;

    float minDis;
    float maxDis;
    float? firstMousePosX;

    bool canMove;

    Animator animator;

    private void OnEnable()
    {
        EventManager.start += StartGame;
        EventManager.stumble += Stumble;
        EventManager.finish += Finish;
    }

    private void OnDisable()
    {
        EventManager.start -= StartGame;
        EventManager.stumble -= Stumble;
        EventManager.finish -= Finish;
    }

    // Start is called before the first frame update
    void Start()
    {
        screenDis = (Screen.width * 7.5f / 10f) - (Screen.width * 2.5f / 10f);

        roadDisX = Mathf.Abs(maxRoadX - minRoadX);

        disDifX = screenDis / roadDisX;

        animator = GetComponent<Animator>();
    }

    void StartGame()
    {
        animator.SetBool("Run", true);
        canMove = true;
    }

    void Stumble()
    {
        animator.SetBool("Run", false);
        animator.SetBool("Stumble", true);
    }

    void Finish()
    {
        animator.SetBool("Run", false);
        animator.SetBool("Stumble", false);
        animator.SetBool("Victory Idle", true);

        Camera.main.GetComponent<Animator>().SetTrigger("Finish");
    }

    // Update is called once per frame
    void Update()
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Stumble"))
        {
            animator.SetBool("Stumble", false);
            animator.SetBool("Run", true);
        }

        if (canMove)
        {
            MouseControlX();
        }
    }
    void MouseControlX()
    {
        if (Input.GetMouseButtonDown(0))
        {
            firstMousePosX = Input.mousePosition.x;
        }
        else if (Input.GetMouseButton(0))
        {
            if (firstMousePosX > Input.mousePosition.x)    // Sol
            {
                minDis = transform.position.x - (((float)firstMousePosX - Input.mousePosition.x) / disDifX);
                if (minDis >= minRoadX)
                {
                    transform.position = new Vector3(minDis, transform.position.y, transform.position.z);
                    firstMousePosX = Input.mousePosition.x;
                }
                else
                {
                    transform.position = new Vector3(minRoadX, transform.position.y, transform.position.z);
                }
            }
            else if (firstMousePosX < Input.mousePosition.x)   // Sa�
            {
                maxDis = transform.position.x + ((Input.mousePosition.x - (float)firstMousePosX) / disDifX);
                if (maxDis <= maxRoadX)
                {
                    transform.position = new Vector3(maxDis, transform.position.y, transform.position.z);
                    firstMousePosX = Input.mousePosition.x;
                }
                else
                {
                    transform.position = new Vector3(maxRoadX, transform.position.y, transform.position.z);
                }
            }
            if (Input.GetMouseButtonDown(0))
            {
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            firstMousePosX = null;
        }
    }    
}